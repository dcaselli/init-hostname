# init-hostname

Change hostname at boot using a configuration file.

This package allows for hostname update from a file located in the `/boot` partition. It was originally meant to change a Raspberry Pi hostname without using any command line but could have other uses, YMMV.

```
# /boot/extra-config.txt

hostname=yourhostname
```
