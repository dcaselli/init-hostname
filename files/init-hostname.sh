#!/bin/sh

set -e

DEFAULT_HOSTNAME="poppy"
EXTRA_CONFIG_PATH=${EXTRA_CONFIG_PATH:-"/boot/extra-config.txt"}

if [ -f "$EXTRA_CONFIG_PATH" ]; then

  DEVICE_HOSTNAME=$(grep 'hostname=.*' $EXTRA_CONFIG_PATH | cut -d= -f2)
  if [ "$DEVICE_HOSTNAME" == "" ]; then
    DEVICE_HOSTNAME=$DEFAULT_HOSTNAME
  fi

  echo "$DEVICE_HOSTNAME" > /etc/hostname

  ADDED_TEXT="# Added by hostname-init"

  # Remove raspberry pi
  sed -i "/raspberrypi/d" /etc/hosts

  # Add or replace new hostname
  sed -i "/.*$ADDED_TEXT/d" /etc/hosts
  sed -i "s/\(127.0.0.1\tlocalhost\)/\1\n127.0.0.1\t$DEVICE_HOSTNAME $ADDED_TEXT/g" /etc/hosts

  ifdown --exclude=lo -a
  ifup --exclude=lo -a

  if [ $(systemctl is-enabled avahi-daemon.service) ]; then
    systemctl restart avahi-daemon.service
  fi

  if [ $(systemctl is-enabled rsyslog.service) ]; then
    systemctl restart rsyslog.service
  fi

  if [ $(systemctl is-enabled systemd-networkd.service) ]; then
    systemctl restart systemd-networkd.service
  fi
fi
